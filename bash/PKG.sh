#!/bin/bash

param=$1
if [ x$param == x ]; then
	echo "try with --help"
	exit 1
fi

if [ x${param} == x--help ]; then
	echo "Modo de uso:"
	echo "   PKG <dir>       tarea, comprime y encripta un directorio"
	echo "   PKG <archivo>   desencripta, descomprime y destarea un archivo"
	exit 0
fi

if [ -f ${param} ]; then
	#param=porn.tar.gz.aes256ecb
	openssl enc -d -aes-256-ecb -pbkdf2 -in ${param} | tar -xvz
	exit 0
fi

if [ -d ${param} ];then
	filename=${param/\/}
	tar -cvz ${filename} | openssl enc -e -aes-256-ecb -pbkdf2 -out ${filename}.tar.gz.aes256ecb
	exit 0;
fi
echo "Algo no va bien"
exit 1;

