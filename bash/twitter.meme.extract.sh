#!/bin/bash
if [ -z $1 ]; then
	echo "Necesito un parámetro que sea un archivo de texto que contenga los MP2 en orden"
	exit 1
fi
if [ ! -f $1 ]; then
	echo "$1 no es un archivo"
	exit 2
fi

e=$( mktemp ./video.XXXX.mp4 )
while read line; do
	wget -O - ${line} >> ${e}
done < $1
