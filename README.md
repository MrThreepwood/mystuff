# myStuff

"Behold...my stuff. I'm particularly fond of these. I pulled 'em out of a place on Midgard called Tex-as. I even named them. Des and Troy. You see, when you put them together...they destroy."

de bash/
========

**HAY_INTERNET**

>    Cuando tu ISP se corta, HAY_INTERNET checkea cuando tu conexión se vuelva a establecer.



**MEM**
>    Un top de las cosas que más memoria te están consumiendo.



**PKG.sh <directorio|archivo encriptado>**

>    Encripta un directorio o desecripta el archivo que le des por parámetro. Te pide una clave. Simple. Nada de cosas complejas como claves publicas y privadas. Ideal para, por ejemplo, hacer un archivo que tengas que enviarlo dentro de un pendrive a traves de mensajería en moto. 



**twitter.meme.extract.sh <archivo>**

>    Cuando te mandan un meme en un twit que está buenísimo y tenés ganas de descargarlo. Generalmente F12 y listo. Pero si es muy largo, ahora todas las redes sociales fraccionan los archivos. Simplemente copiás las URLs de las fracciones en un archivo de texto (uno por linea) y ejecutás el script con el archivo de texto como parámetro. El script te descarga los videos y los concatena.

